import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
  query messageQuery(
    $orderBy: MessageOrderByInput
    $skip: Int
    $first: Int
    $filter: String
  ) {
    messages(orderBy: $orderBy, skip: $skip, first: $first, filter: $filter) {
      count
      messageList {
        id
        text
        likes
        dislikes
        replies {
          id
          text
          likes
          dislikes
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      likes
      dislikes
      createdAt
      replies {
        id
        text
        likes
        dislikes
        createdAt
      }
    }
  }
`;

export const POST_REPLY_MUTATION = gql`
  mutation PostMutation($messageId: ID!, $text: String!) {
    postReply(messageId: $messageId, text: $text) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const UPDATE_MESSAGE_LIKE_MUTATION = gql`
  mutation UpdateMutation($messageId: ID!, $likesCount: Int!) {
    updateMessageLikes(messageId: $messageId, likesCount: $likesCount) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const UPDATE_MESSAGE_DISLIKE_MUTATION = gql`
  mutation UpdateMutation($messageId: ID!, $dislikesCount: Int!) {
    updateMessageDislikes(
      messageId: $messageId
      dislikesCount: $dislikesCount
    ) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const UPDATE_REPLY_LIKE_MUTATION = gql`
  mutation UpdateMutation($replyId: ID!, $likesCount: Int!) {
    updateReplyLikes(replyId: $replyId, likesCount: $likesCount) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const UPDATE_REPLY_DISLIKE_MUTATION = gql`
  mutation UpdateMutation($replyId: ID!, $dislikesCount: Int!) {
    updateReplyDislikes(replyId: $replyId, dislikesCount: $dislikesCount) {
      id
      text
      likes
      dislikes
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
      createdAt
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const NEW_REPLY_SUBSCRIPTION = gql`
  subscription {
    newReply {
      id
      text
      likes
      dislikes
    }
  }
`;

export const UPDATE_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    updateMessage {
      id
      text
      likes
      dislikes
      createdAt
      replies {
        id
        text
        likes
        dislikes
      }
    }
  }
`;
