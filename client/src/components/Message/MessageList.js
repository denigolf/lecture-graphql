import React, { useState } from 'react';
import { Query } from 'react-apollo';
import MessageItem from './MessageItem/MessageItem';
import Preloader from '../Preloader/Preloader';
import MessageInput from './MessageInput/MessageInput';
import {
  MESSAGE_QUERY,
  NEW_MESSAGE_SUBSCRIPTION,
  UPDATE_MESSAGE_SUBSCRIPTION,
} from '../../queries';

const MessageList = (props) => {
  const [orderBy, setOrderBy] = useState('createdAt_ASC');
  const [filter, setFilter] = useState('');
  const [skip, setSkip] = useState(0);
  const [first, setFirst] = useState(10);

  const _subscribeToNewMessages = (subscribeToMore) => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(
          ({ id }) => id === newMessage.id
        );

        if (exists) return prev;
        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  const _subscribeToUpdateMessages = (subscribeToMore) => {
    subscribeToMore({
      document: UPDATE_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(
          ({ id }) => id === newMessage.id
        );

        if (exists) return prev;
        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  return (
    <>
      <label>Order by:</label>

      <select
        name="cars"
        id="cars"
        onChange={(e) => setOrderBy(e.target.value)}
      >
        <option value="createdAt_ASC">Date ASC</option>
        <option value="createdAt_DESC">Date DESC</option>
        <option value="text_ASC">Text ASC</option>
        <option value="text_DESC">Text DESC</option>
        <option value="likes_ASC">Likes count ASC</option>
        <option value="likes_DESC">Likes count DESC</option>
        <option value="dislikes_ASC">Dislikes count ASC</option>
        <option value="dislikes_DESC">Dislikes count DESC</option>
      </select>
      <label>Filter by text (Full message):</label>
      <input
        type="text"
        value={filter}
        onInput={(e) => setFilter(e.target.value)}
      />
      <label>Skip:</label>
      <input
        type="number"
        value={skip}
        onInput={(e) => setSkip(parseInt(e.target.value))}
      />
      <label>First:</label>
      <input
        type="number"
        value={first}
        onInput={(e) => setFirst(parseInt(e.target.value))}
      />
      <Query query={MESSAGE_QUERY} variables={{ orderBy, skip, first, filter }}>
        {({ loading, error, data, subscribeToMore }) => {
          if (loading) return <Preloader />;
          if (error) return <div>{error.message}</div>;
          _subscribeToNewMessages(subscribeToMore);
          _subscribeToUpdateMessages(subscribeToMore);

          const {
            messages: { messageList },
          } = data;

          return (
            <div
              className="product-list"
              style={{
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column',
              }}
            >
              {messageList.map((item) => {
                return (
                  <MessageItem
                    id={item.id}
                    createdAt={item.createdAt}
                    text={item.text}
                    likes={item.likes}
                    dislikes={item.dislikes}
                    replies={item.replies}
                    key={item.id}
                    orderBy={orderBy}
                    skip={skip}
                    first={first}
                    filter={filter}
                  />
                );
              })}
              <MessageInput
                orderBy={orderBy}
                skip={skip}
                first={first}
                filter={filter}
              />
            </div>
          );
        }}
      </Query>
    </>
  );
};

export default MessageList;
