import React, { useState } from 'react';

import './MessageItem.scss';
import ReplyList from '../../Reply/ReplyList';
import { Mutation } from 'react-apollo';

import {
  UPDATE_MESSAGE_LIKE_MUTATION,
  UPDATE_MESSAGE_DISLIKE_MUTATION,
} from '../../../queries';

const Message = (props) => {
  const [isLiked, setIsLiked] = useState(false);
  const [isDisliked, setIsDisliked] = useState(false);

  const { id, text, likes, dislikes, replies } = props;
  const { orderBy, skip, first, filter } = props;

  return (
    <div className="message">
      <img
        className="message-user-avatar"
        src="https://icon-library.com/images/anonymous-avatar-icon/anonymous-avatar-icon-9.jpg"
        alt="user-icon"
      />
      <div className="message-text-container">
        <span className="message-user-name">{id}</span>

        <div className="message-text-button">
          <span className="message-text">{text}</span>

          {isLiked ? (
            <Mutation
              mutation={UPDATE_MESSAGE_LIKE_MUTATION}
              variables={{ messageId: id, likesCount: likes - 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    updateMutation();
                    setIsLiked(false);
                  }}
                >
                  &#128077;
                  {likes}
                </button>
              )}
            </Mutation>
          ) : (
            <Mutation
              mutation={UPDATE_MESSAGE_LIKE_MUTATION}
              variables={{ messageId: id, likesCount: likes + 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    updateMutation();
                    setIsLiked(true);
                  }}
                >
                  &#128077;
                  {likes}
                </button>
              )}
            </Mutation>
          )}

          {isDisliked ? (
            <Mutation
              mutation={UPDATE_MESSAGE_DISLIKE_MUTATION}
              variables={{ messageId: id, dislikesCount: dislikes - 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    setIsDisliked(false);

                    updateMutation();
                  }}
                >
                  &#128078;
                  {dislikes}
                </button>
              )}
            </Mutation>
          ) : (
            <Mutation
              mutation={UPDATE_MESSAGE_DISLIKE_MUTATION}
              variables={{ messageId: id, dislikesCount: dislikes + 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    setIsDisliked(true);

                    updateMutation();
                  }}
                >
                  &#128078;
                  {dislikes}
                </button>
              )}
            </Mutation>
          )}
        </div>
      </div>
      <ReplyList
        id={id}
        replies={replies}
        orderBy={orderBy}
        skip={skip}
        first={first}
        filter={filter}
      />
    </div>
  );
};

export default Message;
