import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { MESSAGE_QUERY, POST_MESSAGE_MUTATION } from '../../../queries';
import { useHistory } from 'react-router-dom';
import './MessageInput.scss';

const MessageInput = (props) => {
  const [text, setText] = useState('');
  const history = useHistory();

  const clearInput = () => {
    setText('');
  };

  const _updateStoreAfterAddingMessage = (store, newMessage) => {
    const { orderBy, skip, first, filter } = props;

    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
        skip,
        first,
        filter,
      },
    });
    data.messages.messageList.push(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  return (
    <div className="message-input">
      <input
        className="message-input-text"
        type="text"
        placeholder="Enter your message..."
        onChange={(e) => setText(e.target.value)}
        value={text}
      />
      <Mutation
        mutation={POST_MESSAGE_MUTATION}
        variables={{ text }}
        update={(store, { data: { postMessage } }) => {
          _updateStoreAfterAddingMessage(store, postMessage);
        }}
        onCompleted={() => {
          clearInput();
          history.push('/');
        }}
      >
        {(postMutation) => {
          if (text.trim()) {
            return (
              <button className="message-input-button" onClick={postMutation}>
                Send
              </button>
            );
          } else {
            return <button className="message-input-button">Send</button>;
          }
        }}
      </Mutation>
    </div>
  );
};

export default MessageInput;
