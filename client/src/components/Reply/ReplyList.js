import React, { useState } from 'react';
import ReplyForm from './ReplyForm';
import ReplyItem from './ReplyItem';

const ReplyList = (props) => {
  const [showReplyForm, toggleForm] = useState(false);
  const { id, replies } = props;
  const { orderBy, skip, first, filter } = props;

  return (
    <div className="reply-list" style={{ border: '1px solid steelblue' }}>
      {replies.length > 0 && <span>Replies:</span>}
      {replies.map((item) => {
        return (
          <ReplyItem
            id={item.id}
            createdAt={item.createdAt}
            text={item.text}
            likes={item.likes}
            dislikes={item.dislikes}
            key={item.id}
          />
        );
      })}
      <button
        className="review-button"
        onClick={() => toggleForm(!showReplyForm)}
      >
        {showReplyForm ? 'Close replies' : 'Add reply'}
      </button>
      {showReplyForm && (
        <ReplyForm
          messageId={id}
          toggleForm={toggleForm}
          orderBy={orderBy}
          skip={skip}
          first={first}
          filter={filter}
        />
      )}
    </div>
  );
};

export default ReplyList;
