import React, { useState } from 'react';

import { Mutation } from 'react-apollo';

import {
  UPDATE_REPLY_LIKE_MUTATION,
  UPDATE_REPLY_DISLIKE_MUTATION,
} from '../../queries';

const ReplyItem = (props) => {
  const [isLiked, setIsLiked] = useState(false);
  const [isDisliked, setIsDisliked] = useState(false);

  const { id, text, likes, dislikes, createdAt } = props;

  return (
    <div className="message">
      <img
        className="message-user-avatar"
        src="https://icon-library.com/images/anonymous-avatar-icon/anonymous-avatar-icon-9.jpg"
        alt="user-icon"
      />
      <div className="message-text-container">
        <span className="message-user-name">{id}</span>

        <div className="message-text-button">
          <span className="message-text">{text}</span>

          {isLiked ? (
            <Mutation
              mutation={UPDATE_REPLY_LIKE_MUTATION}
              variables={{ replyId: id, likesCount: likes - 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    updateMutation();
                    setIsLiked(false);
                  }}
                >
                  &#128077;
                  {likes}
                </button>
              )}
            </Mutation>
          ) : (
            <Mutation
              mutation={UPDATE_REPLY_LIKE_MUTATION}
              variables={{ replyId: id, likesCount: likes + 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    updateMutation();
                    setIsLiked(true);
                  }}
                >
                  &#128077;
                  {likes}
                </button>
              )}
            </Mutation>
          )}

          {isDisliked ? (
            <Mutation
              mutation={UPDATE_REPLY_DISLIKE_MUTATION}
              variables={{ replyId: id, dislikesCount: dislikes - 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    setIsDisliked(false);

                    updateMutation();
                  }}
                >
                  &#128078;
                  {dislikes}
                </button>
              )}
            </Mutation>
          ) : (
            <Mutation
              mutation={UPDATE_REPLY_DISLIKE_MUTATION}
              variables={{ replyId: id, dislikesCount: dislikes + 1 }}
            >
              {(updateMutation) => (
                <button
                  onClick={() => {
                    setIsDisliked(true);

                    updateMutation();
                  }}
                >
                  &#128078;
                  {dislikes}
                </button>
              )}
            </Mutation>
          )}
        </div>
        <span className="message-time">{createdAt}</span>
      </div>
    </div>
  );
};

export default ReplyItem;
