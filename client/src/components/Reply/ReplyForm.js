import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { MESSAGE_QUERY, POST_REPLY_MUTATION } from '../../queries';

const ReplyForm = (props) => {
  const { messageId, toggleForm } = props;
  const [text, setText] = useState('');

  const _updateStoreAfterAddingReply = (store, newReply, messageId) => {
    const { orderBy, first, skip, filter } = props;
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
        first,
        skip,
        filter,
      },
    });
    const repliedProduct = data.messages.messageList.find(
      (item) => item.id === messageId
    );
    repliedProduct.replies.push(newReply);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    toggleForm(false);
  };

  return (
    <div className="reviews-list">
      <input
        type="text"
        placeholder="Reply to message..."
        value={text}
        onChange={(e) => setText(e.target.value)}
      />

      <Mutation
        mutation={POST_REPLY_MUTATION}
        variables={{ messageId, text }}
        update={(store, { data: { postReply } }) => {
          _updateStoreAfterAddingReply(store, postReply, messageId);
        }}
      >
        {(postMutation) => {
          if (text.trim()) {
            return (
              <button className="post-button" onClick={postMutation}>
                Post
              </button>
            );
          } else {
            return <button className="post-button">Post</button>;
          }
        }}
      </Mutation>
    </div>
  );
};

export default ReplyForm;
