const newMessageSubscribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .message({
      mutation_in: ['CREATED'],
    })
    .node();
};

const updateMessageSubscribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .message({
      mutation_in: ['UPDATED'],
    })
    .node();
};

const newReplySubscribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .reply({
      mutation_in: ['CREATED'],
    })
    .node();
};

const updateReplySubscribe = (parent, args, context, info) => {
  return context.prisma.$subscribe
    .reply({
      mutation_in: ['UPDATED'],
    })
    .node();
};

const newMessage = {
  subscribe: newMessageSubscribe,
  resolve: (payload) => payload,
};

const updateMessage = {
  subscribe: updateMessageSubscribe,
  resolve: (payload) => payload,
};

const newReply = {
  subscribe: newReplySubscribe,
  resolve: (payload) => payload,
};

const updateReply = {
  subscribe: updateReplySubscribe,
  resolve: (payload) => payload,
};

module.exports = {
  newMessage,
  updateMessage,
  newReply,
  updateReply,
};
