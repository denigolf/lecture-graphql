const postMessage = (parent, args, context, info) => {
  return context.prisma.createMessage({
    text: args.text,
    likes: 0,
    dislikes: 0,
  });
};

const postReply = async (parent, args, context, info) => {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${args.messageId} does not exist!`);
  }
  return context.prisma.createReply({
    text: args.text,
    likes: 0,
    dislikes: 0,
    message: { connect: { id: args.messageId } },
  });
};

const updateMessageLikes = async (parent, args, context, info) => {
  const updatedMessage = await context.prisma.updateMessage({
    where: { id: args.messageId },
    data: {
      likes: args.likesCount,
    },
  });

  return updatedMessage;
};

const updateMessageDislikes = async (parent, args, context, info) => {
  const updatedMessage = await context.prisma.updateMessage({
    where: { id: args.messageId },
    data: {
      dislikes: args.dislikesCount,
    },
  });

  return updatedMessage;
};

const updateReplyLikes = async (parent, args, context, info) => {
  const updatedReply = await context.prisma.updateReply({
    where: { id: args.replyId },
    data: {
      likes: args.likesCount,
    },
  });

  return updatedReply;
};

const updateReplyDislikes = async (parent, args, context, info) => {
  const updatedReply = await context.prisma.updateReply({
    where: { id: args.replyId },
    data: {
      dislikes: args.dislikesCount,
    },
  });

  return updatedReply;
};

module.exports = {
  postMessage,
  postReply,
  updateMessageLikes,
  updateMessageDislikes,
  updateReplyLikes,
  updateReplyDislikes,
};
